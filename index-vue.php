<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>UEditor 1.4.3.3-vue示例</title>
    <link type="text/css" rel="Stylesheet" href="demo.css" />
    <link type="text/css" rel="Stylesheet" href="WordPaster/js/skygqbox.css" />
	<script type="text/javascript" src="ueditor.config.js" charset="utf-8"></script>
	<script type="text/javascript" src="ueditor.all.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/jquery-1.4.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="WordPaster/js/skygqbox.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/w.js" charset="utf-8"></script>
	<script type="text/javascript" src="zyCapture/z.js" charset="utf-8"></script>
	<script type="text/javascript" src="zyOffice/js/o.js" charset="utf-8"></script>
	<script type="text/javascript" src="vue.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="demo.js" charset="utf-8"></script>
</head>
<body>
	<div id="demos"></div>	
	<div id="ue">
		<ueditor ref="ue"></ueditor>
	</div>
	<script>
        Vue.component('ueditor', {
            data: function(){
                return {
                    editor: null
                }
            },
            props: {
                value: '',
                config: {}
            },
            mounted: function(){
                var pos = window.location.href.lastIndexOf("/");
				var api = [
					window.location.href.substr(0, pos + 1),
					"php/upload.php"
				].join("");
                WordPaster.getInstance({
					//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
					PostUrl: api,
					//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
					ImageUrl: "",
					//设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
					FileFieldName: "file",
					//提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
					ImageMatch: '',
					event:{
						dataReady:function(e){
							//e.word,
							//e.imgs:tag1,tag2,tag3
							console.log(e.imgs)
						}
					}
                });

                //zyCapture
                zyCapture.getInstance({
                    config: {
                        PostUrl: api,
                        FileFieldName: "file",
                        Fields: { uname: "test" }
                    }
                });

                //zyoffice，
                //使用前请在服务端部署zyoffice，
                //http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c
                zyOffice.getInstance({
                    word:"http://localhost:13710/zyoffice/word/convert",
                    wordExport:"http://localhost:13710/zyoffice/word/export",
                    pdf:"http://localhost:13710/zyoffice/pdf/upload"
                });
				
                this.editor = window.UE.getEditor('editor');
                
                //WordPaster快捷键 Ctrl + V
                this.editor.addshortcutkey({
                    "wordpaster": "ctrl+86"
                });
            },
            methods: {
                getUEContent: function(){
                    return this.editor.getContent()
                }
            },
            destroyed: function(){
                this.editor.destroy()
            },
            template: '<div><textarea id="editor" name="editor"/><p>泽优全平台内容发布解决方案 for php ueditor 1.4</p><p>泽优Word一键粘贴控件（WordPaster）</p><p>泽优全平台截屏解决方案（zyCapture）</p><p>泽优Office文档转换服务（zyOffice）</p></div>'
        });

        var ue = new Vue({
            el: '#ue',
            data: {},
            mounted: function () {}
        });

    </script>
</body>
</html>
